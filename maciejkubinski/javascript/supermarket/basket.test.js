
(function testAddFirstProduct() {
    console.log('testAddFirstProduct')
    setupTest()
    
    cartAddProduct('123')
    
    console.assert(cartGetItems()[0].product_id === '123', 'Bad id')
    console.assert(cartGetItems()[0].amount === 1, 'Bad amount')
    console.assert(cartGetItems()[0].subtotal === 100, 'Bad subtotal')
  })();
  
  (function testAddProductTwice() {
    console.log('testAddProductTwice')
    setupTest()
    
    cartAddProduct('123')
    cartAddProduct('123')
  
    console.assert(cartGetItems()[0].product_id === '123', 'Bad id')
    console.assert(cartGetItems()[0].amount === 2, 'Bad amount')
    console.assert(cartGetItems()[0].subtotal === 200, 'Bad subtotal')
  })();
  
  (function testAdd2Products() {
    console.log('testAdd2Products')
    setupTest()
    
    cartAddProduct('123')
    cartAddProduct('234')
  
    console.assert(cartGetItems()[1].product_id === '234', 'Bad id')
    console.assert(cartGetItems()[1].amount === 1, 'Bad amount')
    console.assert(cartGetItems()[1].subtotal === 200, 'Bad subtotal')
  })();
  
  
  (function testRemoveProductTwice() {
    console.log('testAddProductTwice')
    setupTest()
    
    cartAddProduct('123')
    cartAddProduct('123')
    cartAddProduct('234')
    cartRemoveProduct('123')
    cartRemoveProduct('234')
  
    console.assert(cartGetItems()[0].product_id === '123', 'Bad id')
    console.assert(cartGetItems()[0].amount === 1, 'Bad amount')
    console.assert(cartGetItems()[0].subtotal === 100, 'Bad subtotal')
    console.assert(cartGetItems().length === 1, 'Item not removed')
  })();


  (function testGetTotal(){
    console.log('testGetTotal')
    setupTest()

    console.assert(getTotal() === 0, 'Bad total')
    cartAddProduct('123')
    console.assert(getTotal() === 100, 'Bad total')
    cartAddProduct('234')
    console.assert(getTotal() === 300, 'Bad total')
    cartRemoveProduct('123')
    console.assert(getTotal() === 200, 'Bad total')

  })()
  
  
  console.log('End of tests!')
  
  
  function equals(x, y) {
    return JSON.stringify(x) === JSON.stringify(y)
  }
  
  function setupTest() {
    cartReset();
    products = [
      { id: '123', price: 100 },
      { id: '234', price: 200 },
    ];
    console.assert(equals(cartGetItems(), []), 'Cart is not empty')
  }