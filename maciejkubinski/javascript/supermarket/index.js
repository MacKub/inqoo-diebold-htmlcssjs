var products = [
  {
    id: "1",
    name: "Banana Pancakes",
    price: 1690,
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, facilis?",
    promotion: false,
    date_aded: new Date(),
    discount: 0.1,
    category: ["pancakes"],
  },
  {
    id: "2",
    name: "Apple Pancakes",
    price: 1390,
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, facilis?",
    promotion: false,
    date_aded: new Date(),
    category: ["pancakes"],
  },
  {
    id: "3",
    name: "Apple Ice Cream",
    price: 490,
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, facilis?",
    promotion: true,
    date_aded: new Date(),
    discount: 0.15,
    category: ["icecream"],
  },
  {
    id: "4",
    name: "Banana Ice Cream",
    price: 390,
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, facilis?",
    promotion: true,
    date_aded: new Date(),
    category: ["icecream"],
  },
];


var tax = 23;
var shop_discount = 0.05;
var productList = document.getElementById('product-list')


var filter_promoted = false;
var order = 'asc'


function renderToDocument(product) {
var description = getCategoryDescription(product);
var final_price = getFinalPrice(product)

var item = `
<li class="list-group-item d-flex js-productList" >
                <div class="col ">
                  <div class="row"><h4>${product.name}</h4></div>
                  <div class="row">
                      <p>
                        ${description}
                      </p>
                  </div>
                </div>
                <div class="col flex-grow-0 my-auto">
                  <button type="button " class="btn btn-secondary text-nowrap js-addToCart" onClick={getProductById(${product.id})}>
                    Add to cart
                  </button>
                </div>
                <div class="col flex-grow-0 my-auto ms-2">${final_price}$</div>
              </li>
`
productList.innerHTML += item
}

function renderProducts() {
  var count = 0;
  var local_products = order === 'asc' ? products : products.reverse()
  productList.innerHTML = ''

  for (let product of local_products) {

    if (filter_promoted == true && product.promotion == false) continue;

    renderToDocument(product);
  }
}

var promotedBtn = document.getElementById('promoBtn');
promotedBtn.onclick = function(){
  filter_promoted = !filter_promoted
  renderProducts()
}

renderProducts()

function getFinalPrice(product) {
  return (
    Math.round(
      product.price *
        (product.discount
          ? 1 - product.discount
          : product.promotion
          ? 1 - shop_discount
          : 1) *
        (1 + tax / 100)
    ) / 100
  );
}


function getCategoryDescription(product) {
  var description = product.description

  switch (product.category[0]) {
    case 'pancakes':
      description += ' Amerykanskie '; break;
    case 'icecream': description += ' Pyszne lody '; break;
    default: description += ' Inne słodkości';
  }

  return description;
}

var cartItems = [
    
]


function addToCart(product){
  let productExist = false
  for (let cartItem of cartItems){
    if (cartItem.id == product.id){
      cartItem.amount = cartItem.amount + 1
      productExist = true
    }
  }
  if (!productExist){
    cartItems.push({...product, amount: 1})
  }
  console.log(cartItems);
  renderCart()
}


function getProductById(product_id){
  for (let product of products){
    if (product.id == product_id){
      addToCart(product)
      break;
    } else {
      continue;
    }
}
}



var items = document.querySelectorAll(".js-cart-items .js-product")

for (let item of items){
  var closeBtn = item.querySelector('.js-remove-product')
}




function renderCartItem(cartItem) {
  var cartList = document.querySelector(".js-cart-items")
  var final_price = getFinalPrice(cartItem)
  
  var item = `
  <li class="list-group-item d-flex justify-content-between lh-sm js-product align-items-center">

    <h6 class="my-0">${cartItem.name}</h6>

  <div class="d-flex align-items-center">
    <span class="text-muted">${cartItem.amount} * ${final_price}$</span>
    <button type="button" class="btn btn-secondary btn-sm js-remove-product ml-3">Remove</button>
  </div>
</li>
  `
  cartList.innerHTML += item

  updateTotals()
  }

function renderCart(){
  var cartList = document.querySelector(".js-cart-items")
  cartList.innerHTML = ''

  for (let cartItem of cartItems) {
    renderCartItem(cartItem);
  }
}

function updateTotals(){
  var totalItems = 0;
  var totalPrice = 0;

  for (cartItem of cartItems){
    console.log(cartItem);
    console.log(totalItems);
    console.log(totalPrice);
    console.log(cartItem.price);
    console.log(cartItem.amount);
    totalItems = totalItems + cartItem.amount
    totalPrice = totalPrice + cartItem.amount * getFinalPrice(cartItem.price);
  }

}