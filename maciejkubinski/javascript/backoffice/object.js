class ProductEditorView {

    constructor(selector){
        this.el = document.querySelector(selector)
        this.form = this.el.querySelector('form')

        this.form.addEventListener('submit', even => {
            event.preventDefault()

            const data = this.getData()
            fetch('http://localhost:3000/products/' + data.id, {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data)
            }).then(res => res.json()).then(() => searchProduct())
        })
    }
    
    /**
    * @param {Product} data
    */
    setData(data){
        this._data = data
        this.form.elements['name'].value = this._data['name']
        this.form.elements['price'].value = this._data['price']
    }

    getData(){
        this._data['name'] = this.form.elements['name'].value
        this._data['price'] = this.form.elements['price'].value 

        return this._data
    }

}