const productList = document.querySelector("#product-list");

const inputSearch = document.querySelector("#input-search");
inputSearch.addEventListener("keyup", () => {
  searchProduct(inputSearch.value);
});

productList.addEventListener("click", (event) => {
  document.querySelectorAll("#product-list li").forEach((element) => {
    element.classList.remove("active");
  });
  event.target.closest("[data-product-id]").classList.add("active");

  const item = event.target.closest('[data-product-id]')
  if (!item) { return }

  const product_id = item.dataset['productId']

  loadJSON("http://localhost:3000/products/" + product_id).then((product) => {
    form.setData(product)
  });
});

let finalProducts = products;
let productsToRender = finalProducts;

function renderToDocument(product) {
  var item = `
        <li class="list-group-item d-flex justify-content-between align-items-center" data-product-id="${
          product.id
        }">
            <h4>${product.name}</h4>
            <span>${getPrice(product.price)}</span>
        </li>
`;
  productList.innerHTML += item;
}

function renderProducts() {
  productList.innerHTML = "";
  for (let product of productsToRender) {
    renderToDocument(product);
  }
}

function getPrice(price) {
  const finalPrice = `${(price / 100).toFixed(2)} $`;
  return finalPrice;
}

function addProduct(product) {
  finalProducts.push(product);
  renderProducts();
}

function removeProduct(id) {
  const temporaryProducts = finalProducts.filter((product) => {
    return product.id !== id;
  });
  finalProducts = temporaryProducts;
  renderProducts();
}

const form = new ProductEditorView("#product-editor")

// function searchProduct(name){
//     loadJSON('http://localhost:3000/products?q=',(finalProducts) => {
//         if (name){
//             const temporaryProducts = finalProducts.filter((product) => {
//                 return (product.name).toLowerCase().includes(name.toLowerCase())
//             })
//             productsToRender = temporaryProducts
//         } else {
//             productsToRender = finalProducts
//         }

//         renderProducts()
//     } )
// }

function searchProduct(name) {
  loadJSON("http://localhost:3000/products?q=").then((data) => {
    if (name) {
      const temporaryProducts = data.filter((product) => {
        return product.name.toLowerCase().includes(name.toLowerCase());
      });
      productsToRender = temporaryProducts;
    } else {
      productsToRender = data;
    }

    renderProducts();
  });
}

searchProduct();

// function loadJSON(url, callback) {
//     const xhr = new XMLHttpRequest()
//     xhr.open('GET', url)
//     xhr.addEventListener('loadend', event => {
//       const data = JSON.parse(event.target.responseText)
//       callback(data)
//     })
//     xhr.send()
//   }

function loadJSON(url) {
  return new Promise((resolve) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.addEventListener("loadend", (event) => {
      const data = JSON.parse(event.target.responseText);
      resolve(data);
    });
    xhr.send();
    // resolve(url)
  });
}
