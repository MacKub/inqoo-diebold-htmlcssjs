var calc = document.getElementById('calculate')
var monthsInput = document.getElementById('loan_months')
var amountInput = document.getElementById('loan_amount')
var interestInput = document.getElementById('loan_interest')

var totalLoanInterest = 0;


calc.addEventListener('click', calculateLoan)
monthsInput.addEventListener('change', calculateLoan)
amountInput.addEventListener('change', calculateLoan)
interestInput.addEventListener('change', calculateLoan)


function calculateLoan(){
    var months = document.getElementById('loan_months').value
    var loanAmount = document.getElementById('loan_amount').value*100
    var interest = document.getElementById('loan_interest').value / 100
    
    var loanCap = loanAmount / months
    var loanLeft = loanAmount

    var table = document.getElementById('credit-table')
    table.innerHTML = ''
   
    for (let i=1; i <= months; i++){
        var loanLeftBeforePayment = loanLeft
        var loanInterest = loanLeft * interest / 12
        var loanPayment = loanCap + loanInterest
        loanLeft = loanLeft - loanCap
        writeLine(i, loanLeftBeforePayment, loanInterest, loanLeft, loanPayment)
        // updateTotal(totalPayment)
    }
    
}



function writeLine(rate, capital, interest, left, payment){
    var table = document.getElementById('credit-table')
    var line = document.createElement('tr')
    line.innerHTML = `
              <td>${rate}</td>
              <td>${(capital/100).toFixed(2)}</td>
              <td>${(interest/100).toFixed(2)}</td>
              <td>${(left/100).toFixed(2)}</td>
              <td>${(payment/100).toFixed(2)}</td>
    `
    
    table.appendChild(line)
}

// function updateTotal(totalPayment){
//     var tableFoot = document.getElementById('table-foot')

//     console.log(totalPayment);
//     var total = `
//     <tr>
//               <th>Suma</th>
//               <th>${amountInput.value}</th>
//               <th></th>
//               <th></th>
//               <th>${(totalPayment/100).toFixed(2)}</th>
//             </tr>
//         `

//     tableFoot.innerHTML = total
// }