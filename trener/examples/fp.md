// first order function
// var f = function(fn){ return fn }

// closures
// var x = 1; fn = function(){ x }

// immutability
// obj2 = { ...obj1, changed: obj1.changed + 1, nested: { ...obj.nested, x:1 } }


```js

[1,2,3].map(function(x){ return x *2 }) // [2,4,6]
[1,2,3,4].filter(function(x){ return x % 2 == 0 }) // [2,4]
[1,2,3,4,5].reduce(function( sum, x){ return sum + x }, 0) // 15
[{id:1}, {id:2}].find(function( x){ return x.id == 2 }, 0) // {id:2}
[{id:1}, {id:2}].findIndex(function( x){ return x.id == 2 }, 0) // 1

[1,2,3].slice() // [1,2,3]
[1,2,3].slice(1) // [2,3]
[1,2,3].slice(0,1) // [1]
[1,2,3].splice(2,1) // [3] // mutable -> [1,2]
[1,2,3].concat(4) // [1,2,3,4]
[1,2,3].concat([4,5,6]) // [1,2,3,4,5,6]
[1,2,3].concat([[4,5,6]]) // [1,2,3,[4,5,6]]


isEven = function(x) { return x % 2 == 0 };
isNot = function(fn){
    return function(x){ return ! fn(x) }
}
// isOdd = function(x) { return x % 2 !== 0 };
isOdd = isNot(isEven)

mult = function(y){
    return function(x){ return x * y ; }
};

;[1,2,3]
    .map(mult(3))
    .filter(isOdd)

```