
/**
 * @typedef Product
 * @property {number} id
 * @property {string} name
 * @property {number} price_nett
 * @property {string} [description]
 */

/** @type Product[] */
var produts = [
  // { id: '123', name: 'Product Test 123', price_nett: 1230, description: '' },
  // { id: '234', name: 'Product Testing 234', price_nett: 2340, description: '' },
  // { id: '345', name: 'Product Example Test 345', price_nett: 3450, description: '' },
]

class ModelChanged extends Event {
  type = 'model_changed'
}

class ProductsModel extends EventTarget {
  /** 
   * @type Product[]
   * @protected
   */
  _data = [];

  /** 
   * @type Product[]
   * @protected
   */
  _results = this._data

  constructor(products) {
    super()
    this._data = products
    this._results = this._data
    this.filter('')
  }

  /**
   * @returns Promise<Product>
   * @param {string} id 
   */
  async findById(id) {
    const res = await fetch('http://localhost:3000/products/' + id);
    return res.json();
  }
  // findById(id) {
  //   return fetch('http://localhost:3000/products/' + id)
  //     .then(res => res.json())
  // }

  /**
   * 
   * @returns Promise<Product[]>
   */
  getItems() {
    // return loadJSON('http://localhost:3000/products/')
    // return fetch('http://localhost:3000/products/').then(res => res.json())
    return this._results
  }

  /**
   * Save changed product
   * @param {Product} data 
   */
  async saveProduct(data) {
    const res = await fetch('http://localhost:3000/products/' + data.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    });
    const resp = await res.json();
    await this.refresh();
    return resp;
  }
  // saveProduct(data) {
  //   return fetch('http://localhost:3000/products/' + data.id, {
  //     method: 'PUT',
  //     headers: { 'Content-Type': 'application/json' },
  //     body: JSON.stringify(data)
  //   }).then(res => res.json())
  //     .then(resp => {
  //       this.refresh()
  //       return resp
  //     })
  // }

  /**
   * Filer products
   * json-server.cmd ./db.json --static . 
   * @param {string} query 
   */
  async filter(query = '') {
    try {
      this.query = query;
      const res = await fetch('http://localhost:3000/products?q=' + query);
      this._results = await res.json();
      this.dispatchEvent(new ModelChanged('model_changed', this._results));
      
      return this._results
    } catch (error) {
      handleError(error)
    }
  }
  // filter(query = '') {
  //   this.query = query;
  //   return loadJSON('http://localhost:3000/products?q=' + query)
  //     .then(resp => {
  //       this._results = resp;
  //       this.dispatchEvent(new ModelChanged('model_changed', this._results))
  //     }, error => {
  //       handleError(error)
  //     })
  // }

  refresh() {
    return this.filter(this.query)
  }
}

function handleError(error) {

  const alertMessage = document.querySelector('#alertMessage');
  // alertMessage.textContent = error.message;
  alertMessage.textContent = 'Cant load products';
  alertMessage.hidden = false
  setTimeout(() => {
    alertMessage.hidden = true
  }, 3000)
}

function loadJSON(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.open('GET', url)
    xhr.addEventListener('loadend', event => {
      const data = JSON.parse(event.target.responseText)
      resolve(data)
    })
    xhr.addEventListener('error', reject)
    xhr.send()
    // xhr.send(JSON.stringify(data))
  })
}

// const p = loadJSON('products.json'); p.then(console.log)
// loadJSON('products.json').then(console.log)